# requirements.txt
Django==1.5.1
Markdown==2.4.1
stripe==1.9.2
django-jsonfield==0.9.10
south==0.8.1
django-suit==0.2.3
django-debug-toolbar==0.9.4
gunicorn